from kivymd.app import MDApp
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.button import MDRoundFlatIconButton, MDFillRoundFlatButton, MDFloatingActionButton
from kivymd.uix.label import MDLabel
from kivy.storage.jsonstore import JsonStore
from kivy.uix.togglebutton import ToggleButton
from kivymd.uix.screen import MDScreen
from kivymd.uix.selectioncontrol import MDCheckbox
from kivymd.uix.textfield import MDTextField


class ToDoListe(MDApp):
    def build(self):
        self.title = 'To-Do List'
        self.store = JsonStore('taches.json')
        self.layout = MDBoxLayout(orientation="vertical")
        self.theme_cls.theme_style = "Light"
        self.theme_cls.primary_palette = "Blue"
        # On charge la liste de taches ainsi que la partie ajout
        self.chargerTaches()
        return self.layout

    def chargerTaches(self):
        """
            Charge la liste de tâches contenue dans la fichier "taches.json"
        """
        # On vide le box layout pour afficher la liste contenue dans le json
        self.layout.clear_widgets()
        for cle in self.store:

            tachelayout = MDBoxLayout(spacing=10, padding=10, md_bg_color="grey" )
            tache = str(self.store[cle]['tache'])
            labelTache = MDLabel(text=tache, size_hint=(.6, 1))
            btnSuppression = MDFloatingActionButton(icon="trash-can-outline",
                                                   on_press=lambda x, tn=tache: self.suppressionTache(tn),
                                                   padding=5,md_bg_color="e4181b" )
            labelCheckbox = MDLabel(text='Completé' if self.store[cle]['complete'] else 'En cours', size_hint=(.2, 1))
            checkbox = MDCheckbox(text='Completé' if self.store[cle]['complete'] else 'En cours',
                                    state='down' if self.store[cle]['complete'] else 'normal',
                                    on_press=lambda _, tn=tache: self.toggle_task(tn), size_hint=(.2, 1), group="x")

            tachelayout.add_widget(labelTache)
            tachelayout.add_widget(checkbox)
            tachelayout.add_widget(labelCheckbox)
            tachelayout.add_widget(btnSuppression)

            self.layout.add_widget(tachelayout)
        ajoutlayout = MDBoxLayout(spacing=10, padding=10,  md_bg_color="white", adaptive_height=True)
        self.ajoutInput = MDTextField(multiline=False, mode="rectangle", hint_text="Nouvelle tâche", icon_left="note-text")
        self.ajoutBtn = MDFloatingActionButton(icon="plus", on_press=self.ajoutTacheJson, md_bg_color="grey")
        ajoutlayout.add_widget(self.ajoutInput)
        ajoutlayout.add_widget(self.ajoutBtn)
        self.layout.add_widget(ajoutlayout)

    def suppressionTache(self, args):
        """
        Supprime la tache du fichier JSON
        :param args: tache a supprimer
        """
        self.store.delete(args)
        self.chargerTaches()

    def ajoutTacheJson(self, instance):
        """
        Ajoute une tache au fichier JSON
        :param instance:
        """
        ajoutTache = self.ajoutInput.text.strip()
        if ajoutTache:
            self.store.put(ajoutTache, tache=ajoutTache, complete=False)
            self.chargerTaches()

    def toggle_task(self, tache):
        """
        Modifie l'état de la tache
        :param tache: nom de la tache a modifier
        """
        infosTache = self.store.get(tache)
        self.complete = infosTache['complete']
        if infosTache['complete']:
            self.complete = False
        else:
            self.complete = True
        self.store.put(tache, tache=tache, complete=self.complete)
        self.chargerTaches()


if __name__ == '__main__':
    ToDoListe().run()
